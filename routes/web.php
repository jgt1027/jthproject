<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});


$router->group(['prefix' => 'api/member'], function ($router) {

    $router->post('/login', 'MemberController@Login');           //로그인
    $router->post('/create', 'MemberController@Create');         //회원가입
    $router->post('/updatePwd', 'MemberController@UpdatePwd');   //비밀번호 변경
    $router->get('/findPwd', 'MemberController@FindPwd');        //비밀번호 찾기
    $router->post('/send', 'MemberController@SendSms');          //인증문자 전송
    $router->post('/getSms', 'MemberController@GetSms');         //인증 확인
});

$router->group(['prefix' => 'api/configService'], function ($router) {
    $router->get('/read', 'ConfigServiceController@ReadService');//약관 읽기
    $router->post('/create', 'ConfigServiceController@Create');  //약관 생성
    $router->post('/update', 'ConfigServiceController@Update');  //약관 수정
});




