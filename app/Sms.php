<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sms extends Model
{
    public $timestamps = true;
    protected $table = 'dgm_sms';
    protected $primaryKey = 's_idx';

    protected $fillable = [
        's_idx', 's_hp', 's_code', 's_auth'
    ];

    public function GetSms($filters)
    {
        $result = DB::table($this->table . 'as s')
            ->where($filters)->first();
    }
}
