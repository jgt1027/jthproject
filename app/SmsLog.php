<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SmsLog extends Model
{
    public $timestamps = true;
    protected $table = 'dgm_sms_log';
    protected $primaryKey = 'sl_idx';

    public $fillable = [
        'sl_idx', 's_idx', 'sl_code', 'sl_info'
    ];

    public function GetSmsLog($filters)
    {
        $result = DB::table($this->table . 'as sl')
            ->where($filters)->first();
    }
}
