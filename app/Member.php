<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    public $timestamps = true;
    protected $table = 'dgm_member';
    protected $primaryKey = 'mb_no';

    protected $fillable = [
        'mb_no', 'mb_email', 'mb_pwd', 'mb_gender', 'mb_hp', 'mb_last_login',
        'mb_is_use', 'mb_temporary', 'mb_uuid'
    ];

    public function getMember($filters)
    {
        $result = DB::table($this->table . 'as m')
            ->join('dgm_member_type as mt', 'mt.mb_no', '=', 'm.mb_no')
            ->where($filters)->first();
    }
}
