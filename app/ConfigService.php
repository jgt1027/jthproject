<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class ConfigService extends Model {
    protected $table = 'dgm_config_service';
    protected $primaryKey = 'cs_idx';
    public $timestamps= false;

    protected $fillable = [
        'cs_idx','cs_privacy_provide'
    ];

    public function getService($filters){
        $result = DB::table($this->table. 'as cs')
            ->where($filters)->first();
    }
}
