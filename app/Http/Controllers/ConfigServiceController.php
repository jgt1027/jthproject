<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\ConfigService;

class ConfigServiceController extends Controller
{
    public function ReadService(Request $request)
    {
        $Msg = '약관보기';
        $ResultCode = 'SUCCEEDED';
        $Data = array();

        $validator = Validator::make($request->all(), [
            'cs_idx' => 'required'
        ]);
        if ($validator->fails()) {
            $ResultCode = 'FAIL';
            $Data = '약관번호가 비었습니다.';
            $result = compact('Msg', 'ResultCode', 'Data');

            return $result;
        }
        $configservice = new ConfigService();
        $readService = $configservice::where('cs_idx', $request['cs_idx'])->first();

        if (!$readService) {
            $Data['Result']['info']['reason'] = '약관이 존재하지않습니다.';
            $Data['Result']['info']['status'] = 'false';
            $Data['Result']['info']['type'] = '-15';
            $result = compact('Msg', 'ResultCode', 'Data');
            return $result;
        }

        $Data['Result']['List'] = $readService;
        $Data['Result']['info']['reason'] = '약관 조회 성공';
        $Data['Result']['info']['status'] = '약관 조회 성공';
        $Data['Result']['info']['type'] = '1';
        $result = compact('Msg', 'ResultCode', 'Data');

        return $result;

    }

    public function Create(Request $request)
    {
        $Msg = '약관 생성';
        $ResultCode = 'SUCCEEDED';
        $Data = array();

        $validator = Validator::make($request->all(), [
            'cs_privacy_provide' => 'required'
        ]);

        if ($validator->fails()) {
            $ResultCode = 'FAIL';
            $Data = '내용이 없습니다.';
            $result = compact('Msg', 'ResultCode', 'Data');
            return $result;
        }
        $request['cs_idx'] = null;
        DB::beginTransaction();
        $csModel = new ConfigService();
        $cs = $csModel->create($request->input());
        $cs_idx = DB::getPdo()->lastInsertId();
        if (!$cs) {
            DB::rollBack();
            $Data['Result']['info']['reason'] = '약관 생성 실패';
            $Data['Result']['info']['status'] = 'false';
            $Data['Result']['info']['type'] = '-16';
            $result = compact('Msg', 'ResultCode', 'Data');
            return $result;
        }
        DB::commit();
        $Data['Result']['List'] = $cs_idx;
        $Data['Result']['info']['reason'] = '약관 생성 성공';
        $Data['Result']['info']['status'] = 'true';
        $Data['Result']['info']['type'] = '1';
        $result = compact('Msg', 'ResultCode', 'Data');
        return $result;

    }

    public function Update(Request $request)
    {
        $Msg = '약관 수정';
        $ResultCode = 'SUCCEEDED';
        $Data = array();

        $validator = Validator::make($request->all(), [
            'cs_idx' => 'required',
            'cs_privacy_provide' => 'required'
        ]);
        if ($validator->fails()) {
            $Data = '유효성 검사 실패';
            $ResultCode = 'FAIL';
            $result = compact('Msg','ResultCode','Data');
            return $result;
        }

        DB::beginTransaction();
        $cs = ConfigService::where('cs_idx',$request['cs_idx'])->first();
        if(!$cs){
            $Data['Result']['info']['reason'] = '존재하지 않는 약관입니다.';
            $Data['Result']['info']['status'] = 'false';
            $Data['Result']['info']['type'] = '-17';
            $result = compact('Msg', 'ResultCode', 'Data');
            return $result;
        }
        $cs = ConfigService::where('cs_idx',$request['cs_idx'])
            ->update(['cs_privacy_provide'=>$request['cs_privacy_provide']]);
        if(!$cs){
            DB::rollBack();
            $Data['Result']['info']['reason'] = '약관 수정 실패';
            $Data['Result']['info']['status'] = 'false';
            $Data['Result']['info']['type'] = '-18';
            $result = compact('Msg','ResultCode','Data');
            return $result;
        }
        DB::commit();
        $Data['Result']['List'] = $cs;
        $Data['Result']['info']['reason'] = '약관 수정 성공';
        $Data['Result']['info']['status'] = 'true';
        $Data['Result']['info']['type'] = '1';
        $result = compact('Msg','ResultCode','Data');
        return $result;

    }
}
