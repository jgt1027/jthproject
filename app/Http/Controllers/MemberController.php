<?php


namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Helpers\Member\MemberHelper;
use App\Helpers\Member\MemberFactory;
use Illuminate\Support\Facades\DB;
use App\Helpers\Sms\SmsHelper;
use App\Helpers\Sms\Smsfactory;
use App\Sms;

class MemberController extends Controller
{

    public function Login(Request $request)
    {
        $Msg = '로그인';
        $ResultCode = 'SUCCEEDED';
        $Data = array();

        $validator = Validator::make($request->all(), [
            'mb_email' => 'required',
            'mb_pwd' => 'required',
        ]);
        if ($validator->fails()) {
            $ResultCode = 'FAIL';
            $result = compact('msg', 'ResultCode');
            return $result;
        }

        $memberHelper = new MemberHelper();

        $Data['Result'] = $memberHelper->Login($request->input());
        $result = compact('Msg', 'ResultCode', 'Data');

        return response()->json($result);
    }


    public function Create(Request $request)
    {
        $Msg = '회원가입';
        $ResultCode = 'SUCCEEDED';
        $Data = array();
        $validator = Validator::make($request->all(), [
            'mb_email' => 'required|email',
            'mb_pwd' => 'required',
            'mb_gender' => 'required',
            'mb_hp' => 'required'
        ]);
        if ($validator->fails()) {
            $ResultCode = "FAIL";
            $Msg = '값이 잘못되었습니다.';
            $result = compact('Msg', 'ResultCode');
            return $result;
        }

        $inputs = $request->input();
        DB::beginTransaction();
        $memberHelper = new MemberHelper();
        $member = $memberHelper->Create($inputs);

        $inputs['mb_no'] = $member['List'];
        if (!$member) {
            DB::rollBack();
            $Data['Result'] = $member;
            $result = compact('Msg', 'ResultCode', 'Data');
            return response()->json($result);
        }
        DB::commit();
        $Data['Result'] = $member;
        $Data['Result']['List']['mb_no'] = $inputs['mb_no'];
        $Data['Result']['Info'] = '멤버 생성 성공';
        $result = compact('Msg', 'ResultCode', 'Data');
        return response()->json($result);

    }

    public function UpdatePwd(Request $request)
    {
        $Msg = '비밀번호 변경';
        $ResultCode = 'SECCEEDED';
        $Data = array();
        $validator = Validator::make($request->all(), [
            'mb_no' => 'required',
            'mb_pwd' => 'required'
        ]);
        if ($validator->fails()) {
            $ResultCode = 'FAIL';
            $Data = '값이 유효하지 않습니다.';
            $result = compact('Msg', 'ResultCode', 'Data');
            return response()->json($result);
        }

        DB::beginTransaction();
        $memberHelper = new MemberHelper();
        $pwd = $memberHelper->UpdatePwd($request);
        if (!$pwd) {
            DB::rollBack();
            $Data['Result'] = $pwd;
            $result = compact('Msg', 'ResultCode', 'Data');
            return response()->json($result);
        }
        DB::commit();
        $Data['Result'] = $pwd;
        $Data['Result']['List']['mb_no'] = $pwd['List'];
        $Data['Result']['Info'] = '비밀번호 변경 성공';
        $result = compact('Msg', 'ResultCode', 'Data');

        return response()->json($result);


    }

    public function SendSms(Request $request)
    {
        $Msg = '문자 발신';
        $ResultCode = 'SECCEEDED';
        $Data = array();
        $validator = Validator::make($request->all(), [
            's_hp' => 'required'
        ]);
        if ($validator->fails()) {
            $ResultCode = 'FAIL';
            $Data = '번호가 유효하지 않습니다.';
            $result = compact('Msg', 'ResultCode', 'Data');
            return $result;
        }
        DB::beginTransaction();
        $smsModel = new SmsHelper();
        $sendSms = $smsModel->SendSms($request);

        if (!$sendSms) {
            DB::rollBack();
            $Data['Result'] = $sendSms;
            $result = compact('Msg', 'ResultCode', 'Data');
            return $result;
        }
        DB::commit();
        $Data['Result'] = $sendSms;
        $Data['Result']['List']['authcode'] = $sendSms['List']['auth'];
        $Data['Result']['info'] = '인증번호 발신 성공';
        $result = compact('Msg', 'ResultCode', 'Data');
        return response()->json($result);

    }

    public function GetSms(Request $request)
    {
        $Msg = '문자 수신';
        $ResultCode = 'SECCEEDED';
        $Data = array();
        $validator = Validator::make($request->all(), [
            's_auth' => 'required',
            's_code' => 'required',
            's_hp' => 'required'
        ]);
        if ($validator->fails()) {
            $ResultCode = 'FAIL';
            $Data = '인증번호를 입력해주세요';
            $result = compact('Msg', 'ResultCode', 'Data');
            return $result;
        }
        $Sms = new Sms();
        $getSms = $Sms::where('s_hp', $request['s_hp'])
            ->where('s_code', $request['s_code'])
            ->where('s_auth', $request['s_auth'])->first();
        if (!$getSms) {
            $Data['Result']['info']['reason'] = '인증번호가 일치하지 않습니다.';
            $Data['Result']['info']['status'] = 'false';
            $Data['Result']['info']['type'] = '-12';
            $result = compact('Msg', 'ResultCode', 'Data');
            return $result;
        }
        $Data['Result']['info']['reason'] = '인증번호 일치';
        $Data['Result']['info']['status'] = 'true';
        $Data['Result']['info']['type'] = '1';
        $result = compact('Msg', 'ResultCode', 'Data');
        return $result;

    }

    public function FindPwd(Request $request)
    {
        $Msg = '비밀번호 찾기';
        $ResultCode = 'SUCCEEDED';
        $Data = array();

        $validator = Validator::make($request->all(), [
            'mb_email' => 'required|email',
            'mb_hp' => 'required'
        ]);
        if ($validator->fails()) {
            $Data = '유효성 검사 실패';
            $ResultCode = 'FAIL';
            $result = compact('MSg','ResultCode','Data');
            return $result;
        }
        DB::beginTransaction();
        $memberHelper = new MemberHelper();
        $member = $memberHelper->FindPwd($request);
        if(!$member){
            DB::rollBack();
            $Data['Result'] = $member;
            $result = compact('Msg', 'ResultCode', 'Data');
            return $result;
        }
        DB::commit();
        $Data['Result'] = $member;
        $Data['Result']['Info'] = '멤버 생성 성공';
        $result = compact('Msg', 'ResultCode', 'Data');
        return $result;
    }
}
