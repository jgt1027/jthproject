<?php

namespace App\Helpers\Member;

use Firebase\JWT\JWT;
use Illuminate\Support\Facades\DB;
use App\Member;


class MemberHelper
{
    private $Params = array(
        'mb_no' => null,
        'mb_email' => null,
        'mb_pwd' => null,
        'mb_gender' => null,
        'mb_hp' => null,
        'mb_last_login' => null,
        'mb_is_use' => 1,
        'mb_temporary' => 1,
        'mb_uuid' => null
    );

    public static function JwtEncodeMbno($member)
    {
        $payload = [
            'iss' => env('JWT_ISS'),// Issuer of the token
            'mb_no' => $member->mb_no, // Subject of the token
            'mb_type' => $member->mb_type,
            'iat' => time(), // Time when JWT was issued.
            'exp' => time() + env('JWT_EXP'),
        ];
        return JWT::encode($payload, env('JWT_SECRET'));
    }

    public static function SetPassword($password)
    {
        $obj_passwd = DB::select('select password( ? ) as pass', [$password]);
        $new_password = $obj_passwd[0]->pass;
        return $new_password;
    }

    public function SetArrayData($requestInput, $array)
    {
        foreach ($array as $key => $value) {
            if (isset($requestInput[$key])) {
                $array[$key] = $requestInput[$key];
            }
        }
        return $array;
    }


    public function Login($request)
    {
        $ar['List'] = array();

        $id = Member::where('mb_email', $request['mb_email'])->first();
        if (!$id) {
            $ar['info']['reason'] = '입력하신 이메일은 등록되지 않은 이메일 입니다.';
            $ar['info']['status'] = 'false';
            $ar['info']['type'] = '-1';
            return $ar;
        }
        if ($id->mb_is_use == 0) {
            $ar['info']['reason'] = '탈퇴한 회원 입니다.';
            $ar['info']['status'] = 'false';
            $ar['info']['type'] = '-1';
            return $ar;
        }

        $pwd = MemberHelper::SetPassword($request['mb_pwd']);
        $member = Member::where('mb_email', $request['mb_email'])->where('mb_pwd', $pwd)->first();

        if (!$member) {
            $ar['info']['reason'] = '비밀번호가 정확하지 않습니다.';
            $ar['info']['status'] = 'false';
            $ar['info']['type'] = '-1';
            return $ar;
        }

        $status = Member::where('mb_no', $member->mb_no)->update(['mb_last_login' => DATE("Y-m-d :H:i:s")]);
        if (!$status) {
            $ar['info']['reason'] = '데이터베이스 오류';
            $ar['info']['status'] = 'false';
            $ar['info']['type'] = '-1';
            return $ar;
        }

        $ar['info']['reason'] = '로그인 성공';
        $ar['info']['status'] = 'true';
        $ar['info']['type'] = '1';
        $ar['List'] = MemberHelper::JwtEncodeMbno($member);

        return $ar;
    }

    public
    function Create($request)
    {
        $ar['List'] = array();
        $id = Member::where('mb_email', $request['mb_email'])->first();
        if ($id) {
            $ar['info']['reason'] = '등록된회원 입니다.';
            $ar['info']['status'] = 'false';
            $ar['info']['type'] = '-1';
            return $ar;
        }
        $this->Params = self::SetArrayData($request, $this->Params);
        $this->Params['mb_pwd'] = self::SetPassword($request['mb_pwd']);
        $this->Params['mb_uuid'] = DB::raw('UUID()');
        $this->Params['mb_last_login'] = DATE("Y-m-d H:i:s");


        $memberModel = new Member;
        DB::beginTransaction();
        $member = $memberModel->create($this->Params);
        $mb_no = DB::getPdo()->lastInsertId();
        if (!$member) {
            DB::rollBack();
            $ar['Info']['reason'] = '멤버 생성 실패';
            $ar['info']['status'] = 'false';
            $ar['info']['type'] = '-5';
            return $ar;
        }
        DB::commit();
        $ar['List'] = $mb_no;
        $ar['Info']['reason'] = '멤버 생성 성공';
        $ar['info']['status'] = 'true';
        $ar['info']['type'] = '1';

        return $ar;

    }

    public function UpdatePwd($request)
    {
        $ar['List'] = array();

        $member = Member::where('mb_no', $request['mb_no'])->first();

        if (!$member) {
            $ar['info']['reason'] = '등록된 회원이 아닙니다.';
            $ar['info']['status'] = 'false';
            $ar['info']['type'] = '-6';
            return $ar;
        }
        if ($member['mb_is_use'] == 0) {
            $ar['info']['reason'] = '탈퇴한 회원입니다.';
            $ar['info']['status'] = 'false';
            $ar['info']['type'] = '-7';
            return $ar;
        }
        $request['mb_pwd'] = self::SetPassword($request['mb_pwd']);

        if ($member['mb_pwd'] == $request['mb_pwd']) {
            $ar['info']['reason'] = '수정전 비밀번호와 입력한 비밀번호가 같습니다.';
            $ar['info']['status'] = 'false';
            $ar['info']['type'] = '-9';
            return $ar;
        }

        DB::beginTransaction();
        $memberModel = new Member;
        $pwd = $memberModel->where('mb_no', $request['mb_no'])->update(['mb_pwd' => $request['mb_pwd']]);

        if (!$pwd) {
            DB::rollBack();
            $ar['info']['reason'] = '비밀번호 수정 실패';
            $ar['info']['status'] = 'false';
            $ar['info']['type'] = '-8';
            return $ar;
        }
        DB::commit();
        $ar['List'] = $pwd['mb_no'];
        $ar['info']['reason'] = '비밀번호 수정 성공';
        $ar['info']['status'] = 'false';
        $ar['info']['type'] = '1';

        return $ar;

    }

    public function FindPwd($request)
    {
        $ar['List'] = array();

        $mem = Member::where('mb_email', $request['mb_email'])
            ->where('mb_hp', $request['mb_hp'])->first();

        if (!$mem) {
            $ar['info']['reason'] = '비밀 번호 찾기 실패';
            $ar['info']['status'] = 'false';
            $ar['info']['type'] = '-99';
            return $ar;
        }
        $pwd = rand(1000, 90000);
        $pwdauth = self::SetPassword($pwd);
        DB::beginTransaction();
        $member = Member::where('mb_email', $request['mb_email'])
            ->where('mb_hp', $request['mb_hp'])
            ->update(['mb_pwd' => $pwdauth]);

        if (!$member) {
            DB::rollBack();
            $ar['info']['reason'] = '임시비밀번호 저장 실패';
            $ar['info']['status'] = 'false';
            $ar['info']['type'] = '-98';
            return $ar;
        }
        DB::commit();
        $ar['List']['authpwd'] = $pwdauth;
        $ar['List']['mb_no'] = $mem['mb_no'];
        $ar['info']['reason'] = '임시 비밀번호 저장 성공';
        $ar['info']['status'] = 'true';
        $ar['info']['type'] = '1';
        return $ar;
    }
}
