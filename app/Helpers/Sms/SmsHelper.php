<?php

namespace App\Helpers\Sms;

use App\Sms;
use App\SmsLog;
use Firebase\JWT\JWT;
use Illuminate\Support\Facades\DB;
use App\Member\MemberHelper;

class SmsHelper
{
    private $Params = array([
        's_idx' => null,
        's_hp' => null,
        's_code' => null,
        's_auth' => null
    ]);

    private $LogParam = array([
        'sl_idx' => null,
        's_idx' => null,
        'sl_code' => null,
        'sl_info' => null
    ]);

    public function SetCode()
    {
        $this->Params['s_code'] = rand(1000, 9999);
    }

    public function SetAuthCode()
    {
        $this->Params['s_auth'] = rand(10000, 99999);
    }


    public function SendSms($request)
    {
        $ar['List'] = array();

        $this->Params ['s_hp'] = $request['s_hp'];
        $this->SetAuthCode();
        $this->SetCode();

        DB::beginTransaction();


        $smsModel = new Sms();
        $sms = $smsModel->create($this->Params);
        $s_idx = DB::getPdo()->lastInsertId();
        if (!$sms) {
            DB::rollBack();
            $ar['info']['reason'] = '인증번호 전송 실패';
            $ar['info']['status'] = 'false';
            $ar['info']['type'] = '-11';
            return $ar;
        }

        $this->LogParam['s_idx'] = $s_idx;
        $this->LogParam['sl_code'] = $this->Params['s_code'];
        $this->LogParam['sl_info'] = 'send';

        $smsLogModel = new SmsLog();
        $smsLogModel->create($this->LogParam);

        DB::commit();
        $ar['List']['code'] = $this->Params['s_code'];
        $ar['List']['auth'] = $this->Params['s_auth'];
        $ar['info']['reason'] = '인증번호 전송 성공';
        $ar['info']['status'] = 'true';
        $ar['info']['type'] = '1';
        return $ar;
    }

    public function GetSms($request)
    {
        $ar['List'] = array();
        $s_hp = $request['s_hp'];
        $s_code = $request['s_code'];
        $s_auth = $request['s_auth'];
        $getSms = SmsLog::where('s_hp', $s_hp)
            ->where('s_code', $s_code)
            ->where('s_auth', $s_auth)->first();
        DB::beginTransaction();

        if (!$getSms) {
            $ar['info']['reason'] = '인증번호가 일치하지 않습니다.';
            $ar['info']['status'] = 'false';
            $ar['info']['type'] = '-12';
            return $ar;
        }
        $smsLogModel = new SmsLog();
        $smsLogModel->update([''])
    }
}
